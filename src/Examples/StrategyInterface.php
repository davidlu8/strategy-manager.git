<?php
declare(strict_types=1);

namespace StrategyManager\Examples;

interface StrategyInterface
{
    /**
     * @param array $params
     * @return void
     */
    public function operate(array $params): void;

}